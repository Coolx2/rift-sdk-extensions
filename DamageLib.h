#pragma once

#include <map>
#include "SDK Extensions.h"

#pragma region Passives
struct PassiveDamageResult {
	PassiveDamageResult() { this->Valid = false; }
	PassiveDamageResult(float physicalDamage, float magicalDamage, float trueDamage, 
		float physicalDamagePercent, float magicalDamagePercent, float trueDamagePercent)
	{
		this->Valid = true;
		this->PhysicalDamage = std::floor(physicalDamage);
		this->MagicalDamage  = std::floor(magicalDamage);
		this->TrueDamage	 = std::floor(trueDamage);

		this->PhysicalDamagePercent = physicalDamagePercent;
		this->MagicalDamagePercent  = magicalDamagePercent;
		this->TrueDamagePercent		= trueDamagePercent;
	}

	bool Valid;
	float PhysicalDamage;
	float MagicalDamage;
	float TrueDamage;
	float PhysicalDamagePercent;
	float MagicalDamagePercent;
	float TrueDamagePercent;
};

enum PassiveDamageType {
	FlatPhysical,
	FlatMagical,
	FlatTrue,
	PercentPhysical,
	PercentMagical,
	PercentTrue,
};

typedef float(*_PassiveDamage) (AIHeroClient*, AIBaseClient*);
struct DamagePassive {
	PassiveDamageType DamageType;
	_PassiveDamage PassiveDamage;
	const char * Name;	

	DamagePassive(PassiveDamageType damageType, _PassiveDamage passiveDamage, const char * name = NULL) {
		DamageType = damageType;
		PassiveDamage = passiveDamage;
		Name = name;
	}
};
#pragma endregion

class DamageLib : public IDamageLib {
	static std::vector<DamagePassive> StaticPassives;
	static std::vector<DamagePassive> DynamicPassives;

	static PassiveDamageResult GetStaticItemDamage(AIHeroClient* Source, bool MinionTarget);
	static PassiveDamageResult GetDynamicItemDamage(AIHeroClient* Source, AIBaseClient* Target);
	static PassiveDamageResult ComputeItemDamage(AIHeroClient* Source, AIBaseClient* Target);

	static PassiveDamageResult GetStaticPassiveDamage(AIHeroClient* Source, bool MinionTarget);
	static PassiveDamageResult GetDynamicPassiveDamage(AIHeroClient* Source, AIBaseClient* Target);
	static PassiveDamageResult ComputePassiveDamage(AIHeroClient* Source, AIBaseClient* Target);

	static float GetPassivePercentMod(AIBaseClient* Source, AIBaseClient* Target, int Type);
public:
	DamageLib();

	float GetSpellDamage(AIBaseClient* Source, AIBaseClient* Target, unsigned char Slot, SkillStage Stage);
	StaticAttackDamage GetStaticAutoAttackDamage(AIHeroClient* Source, bool MinionTarget);
	float GetAutoAttackDamage(AIBaseClient* Source, AIBaseClient* Target, StaticAttackDamage* StaticDmg = NULL, bool CheckPassives = false);
	float GetAutoAttackDamage(AIBaseClient* Source, AIBaseClient* Target, bool CheckPassives = false);	

	float CalculateMagicalDamage(AIBaseClient * Source, AIBaseClient * Target, float Amount);
	float CalculatePhysicalDamage(AIBaseClient * Source, AIBaseClient * Target, float Amount);

	static float GetAutoAttackDamageInternal(AIBaseClient* Source, AIBaseClient* Target, StaticAttackDamage* StaticDmg, bool CheckPassives);

};






