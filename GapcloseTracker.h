#pragma once

#include "SDK Extensions.h"
#include "DashDatabase.h"


class DashTracker {
	static std::map<unsigned int, GapcloserData> ActiveHeroes;
public:
	static void Init();
	static GapcloserData* GetData(AIHeroClient* Unit);

	static void __cdecl OnCastStart(void* AI, PSDK_SPELL_CAST SpellCast, void* UserData);
	static void __cdecl OnCastEnd(void* AI, PSDK_SPELL_CAST SpellCast, void* UserData);	
};