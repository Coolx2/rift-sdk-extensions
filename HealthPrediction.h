#pragma once

#include <map>
#include <forward_list>

#include "SDK Extensions.h"

class AutoAttack {
public:
	AutoAttack() {};
	AutoAttack(AIBaseClient* Sender, AIBaseClient* Target);
	~AutoAttack() {};

	bool CanRemoveAttack();
	float GetPredictedDamage(int delay);

	enum AttackState {
		None,
		Detected,
		Completed
	} AttackStatus;
	float Damage;
	unsigned int SNetworkId;
	unsigned int NetworkId;
	AIBaseClient* Sender;
	AIBaseClient* Target;
	unsigned int AttackDelay;
	unsigned int AttackCastDelay;
	Vector3 StartPosition;
	unsigned int misNID;
	MissileClient* Missile;
	bool IsMelee;
	unsigned int DetectTime;
	int ExtraDelay;

	int ETA();
	float Distance();

	virtual bool IsValid() = 0;
	virtual int  LandTime() = 0;
	virtual int  TimeToLand() = 0;

	unsigned int  ElapsedTime();
	virtual bool  HasReached() = 0;
};

class Attacks {
public:
	static std::map<unsigned int, std::forward_list<std::shared_ptr<AutoAttack> >> InboundAttacks;

	static std::forward_list<std::shared_ptr<AutoAttack>>& GetInboundAttacks(int TargetID);
	static void AddMeleeAttack(AIBaseClient* sender, AIBaseClient* target, int extraDelay);
	static void AddRangedAttack(AIBaseClient* sender, AIBaseClient* target, MissileClient* mc, int extraDelay);
};

class RangedAttack : public AutoAttack {
public:
	RangedAttack(AIBaseClient* sender, AIBaseClient* target, MissileClient* mc, int extraDelay);

	bool IsValid();
	int  LandTime();
	int  TimeToLand();
	bool HasReached();
	float MissileSpeed;
};

class MeleeAttack : public AutoAttack {
public:
	MeleeAttack(AIBaseClient* sender, AIBaseClient* target, int extraDelay);

	bool IsValid();
	int  LandTime();
	int  TimeToLand();
	bool HasReached();
};

class HealthPred : public IHealthPred {
	static unsigned long LastCleanUp;
public:	
	HealthPred();
	~HealthPred() {};

	static void	__cdecl	Tick(void* UserData);
	static void	__cdecl	Draw(void* UserData);
	static void	__cdecl	DrawMenu(void* UserData);

	static bool	__cdecl	OnCreate(void* Object, unsigned int NetworkID, void* UserData);
	static bool	__cdecl	OnDelete(void* Object, unsigned int NetworkID, void* UserData);
	static void __cdecl OnProcessAttack(void* AI, void* TargetObject, bool StartAttack, bool StopAttack, void* UserData);
	static void __cdecl OnCastSpell(void* AI, PSDK_SPELL_CAST SpellCast, void* UserData);

	unsigned int CalculateMissileTravelTime(AIBaseClient* Target, Vector3* StartPos, float MissileDelay, float MissileSpeed);

	float GetHealthPrediction(AIBaseClient* Target, unsigned int Time, bool SimulateDmg = false);
	float GetDamagePrediction(AIBaseClient* Target, unsigned int Time, bool SimulateDmg = false);
	bool  CanKillMinion(AIBaseClient* Minion, float missileDelay, float missileSpeed);	
};