#pragma once

#include "SDK Extensions.h"

class Control : public IControl {
	static unsigned int LastAction;
public:		
	bool Move();
	bool Move(Vector3* Pos, bool Pet = false);

	bool Attack();
	bool Attack(AttackableUnit* Target, bool Pet = false);

	bool CastSpell(unsigned char Slot, bool Release = false);
	bool CastSpell(unsigned char Slot, AttackableUnit * Target, bool Release = false);
	bool CastSpell(unsigned char Slot, Vector3* Pos, bool Release = false);
	bool CastSpell(unsigned char Slot, Vector3* startPos, Vector3* endPos, bool Release = false);

	bool MoveMouse(Vector3 * Pos);	
};