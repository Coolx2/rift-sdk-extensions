#include "stdafx.h"

#include "SDK Extensions.h"
#include "Template.h"

PLUGIN_SETUP("MyTemplateModule", OnLoad);

SDK_EVENT(void) OnLoad(void* UserData) {
	LOAD_ENVIRONMENT();

	if (Player.PTR() && pSDK && pCore) {
		MyTemplateClass::Init();
		Game::PrintChat(R"(<font color="#832232">Template Loaded.</font>)");
	}
}