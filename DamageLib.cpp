#include "DamageLib.h"

std::vector<DamagePassive> DamageLib::StaticPassives;
std::vector<DamagePassive> DamageLib::DynamicPassives;


#pragma region Items Damage
PassiveDamageResult DamageLib::GetStaticItemDamage(AIHeroClient * Source, bool MinionTarget) {
	PassiveDamageResult result(0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f);

	auto Items{ Source->GetItems() };

	if (Source->HasItem((int)ItemID::NashorsTooth, Items)) {
		result.MagicalDamage += 15.f + 0.15f * Source->GetAbilityPower();
	}

	if (Source->HasItem((int)ItemID::RecurveBow, Items)) {
		result.PhysicalDamage += 15.f;
	}

	if (Source->HasItem((int)ItemID::WitsEnd, Items)) {
		result.MagicalDamage += 42.f;
	}

	if (Source->HasItem((int)ItemID::Sheen, Items) && Source->HasBuff("sheen", true, true)) {
		result.PhysicalDamage += Source->GetBaseAttackDamage();
	}

	if (Source->HasItem((int)ItemID::LichBane, Items) && Source->HasBuff("lichbane", true, true)) {
		result.MagicalDamage += 0.75f * Source->GetBaseAttackDamage() + 0.5f * Source->GetAbilityPower();
	}

	if (Source->HasItem((int)ItemID::TrinityForce, Items) && Source->HasBuff("sheen", true, true)) {
		result.PhysicalDamage += 2.f * Source->GetBaseAttackDamage();
	}

	if (Source->GetBuffCount("itemstatikshankcharge", false, true) == 100) {
		float tmpDmg = 0.f;

		if (Source->HasItem((int)ItemID::KircheisShard, Items)) {
			tmpDmg = 50.f;
		}

		if (Source->HasItem((int)ItemID::StatikkShiv, Items)) {
			constexpr int statikk_dmg[]{ 60, 60, 60, 60, 60, 67, 73, 79, 85, 91, 97, 104, 110, 116, 122, 128, 134, 140 };
			float statikk{ (MinionTarget ? 2.2f : 1.f) * statikk_dmg[Source->GetLevel() - 1] };
			if (statikk > tmpDmg) { tmpDmg = statikk; }
		}

		if (Source->HasItem((int)ItemID::RapidFirecannon, Items)) {
			constexpr int cannon_dmg[]{ 50, 50, 50, 50, 50, 56, 61, 67, 72, 77, 83, 88, 94, 99, 104, 110, 115, 120 };
			float rapid{ (float)cannon_dmg[Source->GetLevel() - 1] };
			if (rapid > tmpDmg) { tmpDmg = rapid; }
		}
		result.MagicalDamage = tmpDmg;
	}

	if (Source->HasItem((int)ItemID::GuinsoosRageblade, Items)) {
		result.MagicalDamage += 15.f;
	}

	if (Source->IsMelee()) {
		if (Source->HasItem((int)ItemID::DeadMansPlate, Items)) {
			result.MagicalDamage += (float)Source->GetBuffCount("DreadnoughtMomentumBuff");
		}
		if (Source->HasItem((int)ItemID::TitanicHydra, Items)) {
			result.PhysicalDamage += Source->HasBuff("itemtitanichydracleavebuff", true, true) ? (40.f + 0.1f * Source->GetHealth().Max) : (5.f + 0.01f * Source->GetHealth().Max);
		}
	}

	return result;
}
PassiveDamageResult DamageLib::GetDynamicItemDamage(AIHeroClient * Source, AIBaseClient * Target) {
	PassiveDamageResult result(0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f);

	auto Items{ Source->GetItems() };
	if (Source->HasItem((int)ItemID::BladeoftheRuinedKing, Items)) {
		float itemDamage{ 0.08f * Target->GetHealth().Current };
		if (Target->IsMinion()) {
			itemDamage = min(itemDamage, 60.f);
		}
		result.PhysicalDamage += max(itemDamage, 15.f);
	}

	if (Source->HasItem((int)ItemID::EnchantmentBloodrazor, Items)) {
		float itemDamage{ 0.04f * Target->GetHealth().Max };
		if (Target->IsMinion()) {
			itemDamage = min(itemDamage, 75.f);
		}
		result.PhysicalDamage += itemDamage;
	}

	return result;
}
PassiveDamageResult DamageLib::ComputeItemDamage(AIHeroClient * Source, AIBaseClient * Target) {
	PassiveDamageResult result{ GetStaticItemDamage(Source, Target->IsMinion()) };
	PassiveDamageResult tmp{ GetDynamicItemDamage(Source, Target) };

	result.PhysicalDamage += tmp.PhysicalDamage;
	result.MagicalDamage  += tmp.MagicalDamage;
	result.TrueDamage	  += tmp.TrueDamage;

	result.PhysicalDamagePercent += tmp.PhysicalDamagePercent;
	result.MagicalDamagePercent  += tmp.MagicalDamagePercent;
	result.TrueDamagePercent	 += tmp.TrueDamagePercent;	

	return result;
}
#pragma endregion

#pragma region Passives Damage
PassiveDamageResult DamageLib::GetDynamicPassiveDamage(AIHeroClient * Source, AIBaseClient * Target) {
	PassiveDamageResult result(0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f);	

	for (auto passive : DynamicPassives) {
		if ((passive.Name != NULL && strcmp(Source->GetCharName(), passive.Name) != 0)) {
			continue;
		}

		auto passiveDamage = passive.PassiveDamage(Source, Target);
		switch (passive.DamageType) {
		case PassiveDamageType::FlatPhysical:
			result.PhysicalDamage += passiveDamage;
			break;
		case PassiveDamageType::FlatMagical:
			result.MagicalDamage += passiveDamage;
			break;
		case PassiveDamageType::FlatTrue:
			result.TrueDamage += passiveDamage;
			break;
		case PassiveDamageType::PercentPhysical:
			result.PhysicalDamagePercent *= passiveDamage;
			break;
		case PassiveDamageType::PercentMagical:
			result.MagicalDamagePercent *= passiveDamage;
			break;
		case PassiveDamageType::PercentTrue:
			result.TrueDamagePercent *= passiveDamage;
			break;
		}
	}
	return result;
}
PassiveDamageResult DamageLib::GetStaticPassiveDamage(AIHeroClient * Source, bool MinionTarget) {
	PassiveDamageResult result(0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f);

	for (auto passive : StaticPassives) {
		if ((passive.Name != NULL && strcmp(Source->GetCharName(), passive.Name) != 0)) {
			continue;
		}

		auto passiveDamage = passive.PassiveDamage(Source, NULL);
		switch (passive.DamageType) {
			case PassiveDamageType::FlatPhysical:
				result.PhysicalDamage += passiveDamage;
				break;
			case PassiveDamageType::FlatMagical:
				result.MagicalDamage += passiveDamage;
				break;
			case PassiveDamageType::FlatTrue:
				result.TrueDamage += passiveDamage;
				break;
			case PassiveDamageType::PercentPhysical:
				result.PhysicalDamagePercent *= passiveDamage;
				break;
			case PassiveDamageType::PercentMagical:
				result.MagicalDamagePercent *= passiveDamage;
				break;
			case PassiveDamageType::PercentTrue:
				result.TrueDamagePercent *= passiveDamage;
				break;
		}
	}	
	return result;
}
PassiveDamageResult DamageLib::ComputePassiveDamage(AIHeroClient * Source, AIBaseClient * Target) {
	PassiveDamageResult result{ GetStaticPassiveDamage(Source, Target->IsMinion()) };
	PassiveDamageResult tmp{ GetDynamicPassiveDamage(Source, Target) };

	result.PhysicalDamage	+= tmp.PhysicalDamage;
	result.MagicalDamage	+= tmp.MagicalDamage;
	result.TrueDamage		+= tmp.TrueDamage;

	result.PhysicalDamagePercent *= tmp.PhysicalDamagePercent;
	result.MagicalDamagePercent  *= tmp.MagicalDamagePercent;
	result.TrueDamagePercent	 *= tmp.TrueDamagePercent;

	return result;
}
#pragma endregion

float DamageLib::GetPassivePercentMod(AIBaseClient * Source, AIBaseClient * Target, int Type) {
	double DamageMod;
	if (Type == 0) {
		double totalArmor = Target->GetArmor();

		double lethality = 0.0;
		if (Source->IsHero()) {
			AIHeroClient* tTarg = (AIHeroClient*)Source;
			lethality = tTarg->GetLethality() * (0.6 + 0.4 * tTarg->GetLevel());
		}

		double reducedArmor = totalArmor * Source->GetPercentArmorPen() - Source->GetFlatArmorPen() - lethality;
		if (Source->IsMinion()) {
			reducedArmor = totalArmor;
		}
		else if (Source->IsTurret()) {
			reducedArmor = totalArmor * 0.7;
		}

		if (totalArmor < 0.0) {
			DamageMod = 2.0f - 100.0f / (100.0f - totalArmor);
		}
		else {
			DamageMod = (reducedArmor < 0) ? 1.0f : 100.0f / (100.0f + reducedArmor);
		}
	}
	else if (Type == 1) {
		float totalSpellBlock = Target->GetMagicResist();
		float reducedSpellBlock = totalSpellBlock * Source->GetPercentMagicPen() - Source->GetFlatMagicPen();

		if (totalSpellBlock < 0.0f) {
			DamageMod = 2.0f - 100.0f / (100.0f - totalSpellBlock);
		}
		else {
			DamageMod = (reducedSpellBlock < 0) ? 1.0f : 100.0f / (100.0f + reducedSpellBlock);
		}
	}
	else {
		return 0.0f;
	}

	if (Target->IsMinion() && Target->HasBuff("exaltedwithbaronnashorminion")) {
		auto Minion = (AIMinionClient*)Target;
		if (Source->IsHero() && !Minion->IsSuperMinion() && !Minion->IsSiegeMinion()) {
			DamageMod *= 0.3;
		}
	}

	else if (Source->IsHero()) {
		if (Target->IsMinion()) {
			if (Source->HasBuff("barontarget") && strstr(Target->GetCharName(), "SRU_Baron")) {
				DamageMod *= 0.5;
			}
			if (Source->HasBuff("dragonbuff_tooltipmanager") && Target->HasBuff("s5_dragonvengeance") && strstr(Target->GetCharName(), "SRU_Dragon")) {
				DamageMod *= (1 - 7 / 100 * Source->GetBuff("dragonbuff_", true, true).Stacks);
			}
		}
	}
	return (float)DamageMod;
}

StaticAttackDamage DamageLib::GetStaticAutoAttackDamage(AIHeroClient * Source, bool MinionTarget) {
	if (!Source) { return StaticAttackDamage(); }
	StaticAttackDamage result{ 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };

	auto pDmg{ GetStaticPassiveDamage(Source, MinionTarget) };
	result.RawPhysical	+= ((pDmg.PhysicalDamage + Source->GetAttackDamage())	* pDmg.PhysicalDamagePercent);
	result.RawMagical	+= (pDmg.MagicalDamage	* pDmg.MagicalDamagePercent);
	result.RawTrue		+= (pDmg.TrueDamage		* pDmg.TrueDamagePercent);	

	auto iDmg{ GetStaticItemDamage(Source, MinionTarget) };
	result.RawPhysical	+= (iDmg.PhysicalDamage	* iDmg.PhysicalDamagePercent);
	result.RawMagical	+= (iDmg.MagicalDamage	* iDmg.MagicalDamagePercent);
	result.RawTrue		+= (iDmg.TrueDamage		* iDmg.TrueDamagePercent);

	result.RawTotal = result.RawPhysical + result.RawMagical + result.RawTrue;
	return result;
}

float DamageLib::GetAutoAttackDamage(AIBaseClient * Source, AIBaseClient * Target, StaticAttackDamage* StaticDmg, bool CheckPassives) {
	return GetAutoAttackDamageInternal(Source, Target, StaticDmg, CheckPassives);
}
float DamageLib::GetAutoAttackDamage(AIBaseClient * Source, AIBaseClient * Target, bool CheckPassives) {
	return GetAutoAttackDamageInternal(Source, Target, NULL, CheckPassives);
}
float DamageLib::GetAutoAttackDamageInternal(AIBaseClient* Source, AIBaseClient* Target, StaticAttackDamage* StaticDmg, bool CheckPassives) {
	if (!Source || !Target) {return 0.0f;}

	if (Source->IsTurret() && Target->IsMinion()) {
		float dmg = 0.0f;

		if (Target->IsMelee()) {
			dmg = Target->GetHealth().Max * 0.45f;
		}
		else {
			auto Minion = (AIMinionClient*)Target;
			if (Minion->IsSiegeMinion()) {
				AITurretClient* Turret = (AITurretClient*)Source;
				switch (Turret->GetTurretPos()) {
				case (TURRET_POSITION_OUTER):
					dmg = Target->GetHealth().Max * 0.14f;
					break;
				case (TURRET_POSITION_INNER):
					dmg = Target->GetHealth().Max * 0.11f;
					break;
				default:
					dmg = Target->GetHealth().Max * 0.08f;
				}
			}
			else if (Minion->IsSuperMinion()) {
				dmg = Target->GetHealth().Max * 0.05f;
			}
			else {
				dmg = Target->GetHealth().Max * 0.7f;
			}
		}
		return dmg;
	}

	float dmgPhysical	= (StaticDmg != NULL ? StaticDmg->RawPhysical	: Source->GetAttackDamage());
	float dmgMagical	= (StaticDmg != NULL ? StaticDmg->RawMagical	: 0.0f);
	float dmgTrue		= (StaticDmg != NULL ? StaticDmg->RawTrue		: 0.0f);

	float dmgReduce			= 1.0f;
	float dmgPhysicalToAll	= 1.0f;
	float dmgMagicalToAll	= 1.0f;
	float secondAttack		= 0.0f;

	AIHeroClient* HeroSource = (Source->IsHero()) ? (AIHeroClient*)pSDK->EntityManager->GetObjectFromPTR(Source->PTR()) : NULL;
	AIHeroClient* HeroTarget = (Target->IsHero()) ? (AIHeroClient*)pSDK->EntityManager->GetObjectFromPTR(Target->PTR()) : NULL;

	if (HeroSource) {
		if (Source->IsMelee() && Target->IsMinion() && !Target->IsMonster() && Source->GetBuffCount("talentreaperdisplay", true, true) > 0) {
			auto Items{ HeroSource->GetItems() };
			if (HeroSource->HasItem((int)ItemID::RelicShield, Items)) {
				float itemDamage{ 195.0f + 5.0f * HeroSource->GetLevel() };
				if (itemDamage > Target->GetHealth().Current) { return itemDamage; }
			}
			else if (HeroSource->HasItem((int)ItemID::TargonsBrace, Items)) {
				float itemDamage{ 200.0f + 15.0f * HeroSource->GetLevel() };
				if (itemDamage > Target->GetHealth().Current) { return itemDamage; }
			}
			else if (HeroSource->HasItem((int)ItemID::RemnantOfTheAspect, Items)) {
				float itemDamage{ 320.0f + 30.0f * HeroSource->GetLevel() };
				if (itemDamage > Target->GetHealth().Current) { return itemDamage; }
			}
		}

		if (Source->GetNetworkID() == Player.GetNetworkID() && strcmp(Source->GetCharName(), "Azir") == 0) {
			int soldierCount = 0;
			for (auto &[_, soldier] : pSDK->EntityManager->GetAzirSoldiers(1000.0f, &(Source->GetPosition()))) {
				if (soldier->IsAlive()) {
					++soldierCount;
				}
			}
			if (soldierCount > 0) {
				constexpr int azir_soldier_dmg[]{ 60, 62, 64, 66, 68, 70, 72, 75, 80, 85, 90, 100, 110, 120, 130, 140, 150, 160 };
				dmgMagical += (azir_soldier_dmg[min(HeroSource->GetLevel(), 18) - 1] + Source->GetAbilityPower() * 0.6f) * (soldierCount * 0.25f + 0.75f);
			}
		}

		if (HeroSource->HasBuff("RenektonPreExecute")) {
			float hits = (Source->GetResource(0).Current > 50) ? 3.0f : 2.0f;
			float wDmg = (-5.0f + 10.0f * Source->GetSpell(1).Level) + (0.75f * Source->GetAttackDamage());
			dmgPhysical = wDmg * hits;
		}

		if (CheckPassives) {
			PassiveDamageResult passiveDamage{ StaticDmg != NULL ? GetDynamicPassiveDamage(HeroSource, Target) : ComputePassiveDamage(HeroSource, Target) };
			dmgPhysical += (float)passiveDamage.PhysicalDamage;
			dmgMagical	+= (float)passiveDamage.MagicalDamage;
			dmgTrue		+= (float)passiveDamage.TrueDamage;

			dmgPhysical *= (float)passiveDamage.PhysicalDamagePercent;
			dmgMagical	*= (float)passiveDamage.MagicalDamagePercent;
			dmgTrue		*= (float)passiveDamage.TrueDamagePercent;
		}		

		if (Target->IsMinion()) {
			if (HeroSource->HasItem((int)ItemID::DoransShield)) {
				dmgPhysical += 5.0f;
			}

			if (!Source->IsMelee() && Target->GetTeamID() == 300 &&
				strstr(Target->GetCharName(), "SRU_RiftHerald") != NULL) {
				dmgReduce *= 0.65f;
			}
		}

		auto itemDamage{ StaticDmg != NULL ? GetDynamicItemDamage(HeroSource, Target) : ComputeItemDamage(HeroSource, Target) };
		dmgPhysical += itemDamage.PhysicalDamage;
		dmgMagical  += itemDamage.MagicalDamage;

	}

	if (HeroTarget) {
		if (!Source->IsTurret() && HeroTarget->HasItem((int)ItemID::NinjaTabi)) {
			dmgReduce *= 0.9f;
		}
	}	

	dmgPhysical = pSDK->DamageLib->CalculatePhysicalDamage(Source, Target, dmgPhysical);
	dmgMagical  = pSDK->DamageLib->CalculateMagicalDamage (Source, Target, dmgMagical);
	
	auto dmg = std::floor(dmgPhysical + dmgMagical) * dmgReduce + dmgTrue;
	return dmg > 0.0f ? dmg : 0.0f;
}



float DamageLib::GetSpellDamage(AIBaseClient * Source, AIBaseClient * Target, unsigned char Slot, SkillStage Stage) {
	return 0.0f;
}

float DamageLib::CalculateMagicalDamage(AIBaseClient* Source, AIBaseClient* Target, float Amount) {
	if (Amount <= 0) {
		return 0.0f;
	}
	if (!Source || !Source->IsValid()) {
		SdkUiConsoleWrite("Invalid Source on %s", __FUNCTION__);
		return 0.0f;
	}
	if (!Target || !Target->IsValid()) {
		SdkUiConsoleWrite("Invalid Target on %s", __FUNCTION__);
		return 0.0f;
	}

	float dmg = (DamageLib::GetPassivePercentMod(Source, Target, 1) * Amount);
	dmg = (dmg < 0) ? 0 : dmg;

	if (Target->HasBuff("cursedtouch")) {
		return dmg + Amount * 0.1f;
	}
	return dmg;
}
float DamageLib::CalculatePhysicalDamage(AIBaseClient* Source, AIBaseClient* Target, float Amount) {
	if (Amount <= 0) {
		return 0.0f;
	}
	if (!Source || !Source->IsValid()) {
		SdkUiConsoleWrite("Invalid Source on %s", __FUNCTION__);
		return 0.0f;
	}
	if (!Target || !Target->IsValid()) {
		SdkUiConsoleWrite("Invalid Target on %s", __FUNCTION__);
		return 0.0f;
	}

	float dmg = (DamageLib::GetPassivePercentMod(Source, Target, 0) * Amount);
	dmg = (dmg < 0) ? 0 : dmg;

	return dmg;
}

//Credits to Lexxes for the Passives
#pragma region Passives

DamageLib::DamageLib() {		

	// Red Buff
	DynamicPassives.push_back(DamagePassive{
		FlatTrue,
		[](AIHeroClient* source,AIBaseClient* target) {
			if (source->HasBuff("BlessingoftheLizardElder") && !target->HasBuff("blessingofthelizardelderslow")) {
				return 2.0f + 2.0f * source->GetLevel();				
			}
			return 0.0f;
		}
	});

	// Item Ardent Censer casted on source (based on buffed unit level)
	StaticPassives.push_back(DamagePassive{
		FlatMagical,
		[](AIHeroClient* source, AIBaseClient* target) {
			if (source->HasBuff("itemangelhandbuff")) {
				return 5.0f + (0.8823529411764706f * (source->GetLevel() - 1));
			}
			return 0.0f;
		}				
	});
	
	if (Game::IsHeroInGame("Aatrox")) {
		DynamicPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("aatroxpassivereadyr")) {
					float aatroxPassiveDamage = 0.08f + 0.0047f * source->GetLevel();
					if (target->IsMinion() && target->GetTeamID() == TEAM_JUNGLE) {
						return min(400, aatroxPassiveDamage);
					}
					return aatroxPassiveDamage;
				}
				return 0.0f;
			},
			"Aatrox"
		});
	}

	if (Game::IsHeroInGame("Akali")) {
		StaticPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("akalipweapon")) {
					auto lvl = source->GetLevel();
					float baseDmg = 36.0f;
					if (lvl < 9) {
						baseDmg += lvl * 3.0f;
					}
					else if (lvl < 14) {
						baseDmg += 8.0f * 3.0f + (lvl - 8) * 9.0f;
					}
					else {
						baseDmg += 8.0f * 3.0f + 5.0f *9.0f + (lvl - 13) * 15.0f;
					}
					return baseDmg + 0.9f * source->GetBonusAttackDamage() + 0.7f* source->GetAbilityPower();
				}
				return 0.0f;
			},
			"Akali"		
		});
	}

	if (Game::IsHeroInGame("Alistar")) {
		DynamicPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (target->HasBuff("alistareattack")) {
					return 40.0f + 15.0f * source->GetLevel();
				}
				return 0.0f;
			},
			"Alistar"
		});
	}

	if (Game::IsHeroInGame("Ashe")) {
		DynamicPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				 if (target->HasBuff("ashepassiveslow")) {
					 return (0.1f + source->GetCritChance()) * source->GetAttackDamage();
				 }
				 return 0.0f;
			},
			"Ashe"
		});
	}

	if (Game::IsHeroInGame("Bard")) {
		StaticPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->GetBuff("bardpspiritammocount").Count > 0) {
					return 40.0f + 15.0f * (source->GetBuff("bardpdisplaychimecount").Count / 5.0f) + 0.3f * source->GetAbilityPower();
				}
				return 0.0f;
			},
			"Bard",
		});
	}

	if (Game::IsHeroInGame("Blitzcrank")) {
		StaticPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("powerfist")) {
					return source->GetAttackDamage();
				}
				return 0.0f;
			},
			"Blitzcrank",
		});
	}

	if (Game::IsHeroInGame("Braum")) {
		DynamicPassives.push_back(DamagePassive{
			PassiveDamageType::FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (target->HasBuff("braummarkstunreduction")) {
					return 3.2f + 2.0f * source->GetLevel();
				}
				return 0.0f;
			},
			"Braum"
		});

		DynamicPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (target->GetBuffCount("braummarkcounter") == 3) {
					return 16.0f + 10.0f * source->GetLevel();
				}
				return 0.0f;
			}
		});
	}

	if (Game::IsHeroInGame("Caitlyn")) {
		DynamicPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("caitlynheadshot", false) || source->HasBuff("caitlynheadshotrangecheck") && target->HasBuff("caitlynyordletrapinternal")) {
					auto multiplier = 1.0f;
					auto bonustrappeddmg = 0.0f;
					auto ctritmod = source->GetCritChance();

					if (target->IsHero()) {
						multiplier = source->GetLevel() < 7 ? 0.5f : source->GetLevel() < 13 ? 0.75f : 1.0f;
						int wLevel = target->GetSpell((unsigned char)SpellSlot::W).Level;
						if (target->HasBuff("caitlynyordletrapinternal") && wLevel > 0) {
							bonustrappeddmg = -10.0f + wLevel * 50.0f + 0.25f + wLevel * 0.15f * source->GetAttackDamage();
						}
					}
					else if (target->IsTurret()) {
						ctritmod = min(ctritmod, 0.4f);
					}
					return (multiplier + ctritmod * 1.25f) * source->GetAttackDamage() + bonustrappeddmg;
				}
				return 0.0f;
			},
			"Caitlyn"
		});
	}

	if (Game::IsHeroInGame("Camille")) {
		// Camille Q base
		StaticPassives.push_back(DamagePassive{
		FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("CamilleQ") || source->HasBuff("CamilleQ2") && !source->HasBuff("camilleqprimingcomplete")) {
					return (0.15f + 0.05f * source->GetSpell((unsigned char)SpellSlot::Q).Level) * source->GetAttackDamage();
				}
				return 0.0f;
			},
			"Camille",
		});

		// Camille Q Empowered Physical dmg
		StaticPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("CamilleQ2") && !source->HasBuff("camilleqprimingcomplete")) {
					float empowereddmg = (0.3f + 0.1f * source->GetSpell((unsigned char)SpellSlot::Q).Level) * source->GetAttackDamage();
					float dmgpercent = 1 - (0.36f + 0.04f * min(source->GetLevel(), 16));
					return empowereddmg * 2 * dmgpercent;
				}
				return 0.0f;
			},
			"Camille",
		});

		// Camille Q Empowered true dmg
		StaticPassives.push_back(DamagePassive{
			FlatTrue,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("CamilleQ2") && !source->HasBuff("camilleqprimingcomplete")) {
					float empowereddmg = (0.3f + 0.1f * source->GetSpell((unsigned char)SpellSlot::Q).Level) * source->GetAttackDamage();
					float dmgpercent   = 0.36f + 0.04f * min(source->GetLevel(), 16);
					return empowereddmg * 2 * dmgpercent;
				}
				return 0.0f;
			},
			"Camille",
		});

		// Camille R
		DynamicPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("camiller")) {
					int rLvl = source->GetSpell((unsigned char)SpellSlot::R).Level;
					return 5 * rLvl + 0.02f + 0.02f * rLvl * target->GetHealth().Current;
				}
				return 0.0f;
			},
			"Camille"
		});
	}

	if (Game::IsHeroInGame("Chogath")) {
		DynamicPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("VorpalSpikes")) {
					float baseDmg = 10.f + 10.f * source->GetSpell(2).Level + 0.3f * source->GetAbilityPower();
					float feastDmg = 0.03f + 0.005f * source->GetBuffCount("Feast") * target->GetHealth().Max;
					return baseDmg + feastDmg;
				}
				return 0.0f;
			},
			"ChoGath"
		});
	}

	if (Game::IsHeroInGame("Corki")) {
		// Corki FlatMagical
		StaticPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				return 0.8f * source->GetAttackDamage();
			},
			"Corki"
		});

		// Corki PercentPhysical
		StaticPassives.push_back(DamagePassive{
			PercentPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				return 0.2f;
			},
			"Corki"			
		});
	}

	if (Game::IsHeroInGame("Darius")) {
		StaticPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("dariusnoxiantacticsonh")) {
					return  (0.45f + 0.05f * source->GetSpell((unsigned char)SpellSlot::W).Level) * source->GetAttackDamage();
				}
				return 1.0f;
			},
			"Darius",
		});
	}

	if (Game::IsHeroInGame("Diana")) {
		StaticPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("dianaarcready")) {
					return std::vector<int>{ 20, 25, 30, 35, 40, 50, 60, 70, 80, 90, 105, 120, 135, 155, 175, 200, 225, 250 }[min(source->GetLevel() - 1, 17)] + 0.8f * source->GetAbilityPower();
				}
				return 0.0f;
			},
			"Diana",
		});
	}

	if (Game::IsHeroInGame("Draven")) {
		StaticPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("DravenSpinningAttack")) {
					auto qLvl = source->GetSpell(0).Level;
					return 30.f + 5.f * qLvl + (0.55f + 0.1f * qLvl) * source->GetBonusAttackDamage();
				}
				return 0.f;
			},
			"Draven"
		});
	}

	if (Game::IsHeroInGame("DrMundo")) {
		StaticPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("masochism")) {
					return (0.025f + 0.005f * source->GetSpell((unsigned char)SpellSlot::E).Level) * source->GetHealth().Max;
				}
				return 0.0f;
			},
			"DrMundo",
		});
	}

	if (Game::IsHeroInGame("Ekko")) {
		DynamicPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (target->GetBuffCount("ekkostacks") > 2)
				{
					float ekkopassiveDmg = std::vector<int>{ 30, 40, 50, 60, 70, 80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140 }[min(source->GetLevel() - 1, 17)] + 0.8f * source->GetAbilityPower();
					if (target->IsMinion() && target->GetTeamID() == TEAM_JUNGLE) {
						return min(600,ekkopassiveDmg * 2);
					}
					return ekkopassiveDmg;
				}
				return 0.0f;
			},
			"Ekko"
		});

		// Ekko W Passive
		DynamicPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				int wLvl = source->GetSpell((unsigned char)SpellSlot::W).Level;
				if (target->GetHealth().Current / target->GetHealth().Max < 0.3f && source->GetSpell(1).Learned) {
					float missingHealth = target->GetHealth().Max - target->GetHealth().Current;
					float ekkopassiveDmg = missingHealth * (0.03f + 0.03f * floor(source->GetAbilityPower() / 100));
					if (target->IsHero()) {
						return min(15, ekkopassiveDmg);
					}
					if (target->IsMinion()) {
						return max(150, min(15, ekkopassiveDmg));
					}
				}
				return 0.0f;
			},
			"Ekko"
		});

		// Ekko E
		StaticPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("ekkoeattackbuff")) {
					return 15.0f + 25.0f * source->GetSpell((unsigned char)SpellSlot::W).Level + 0.4f * source->GetAbilityPower();
				}
				return 0.0f;
			},
			"Ekko",
		});
	}

	if (Game::IsHeroInGame("Fizz")) {
		DynamicPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				//TODO: need to check if that is correct
				float basedmg = std::vector<float>{ 3.3333f, 5.5f, 6.6666f, 8.3333f, 10.0f }[max(source->GetSpell((unsigned char)SpellSlot::W).Level - 1, 0)] + 0.0666f * source->GetAbilityPower();
				if (target->HasBuff("FizzW")) {
					return basedmg * 3;
				}
				if (target->HasBuff("fizzonhitbuff")) {
					return basedmg;
				}
				return 0.0f;
			},
			"Fizz"
		});
	}

	if (Game::IsHeroInGame("Galio")) {
		StaticPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("galiopassivebuff")) {
					float baseMResist = 32.0f + 1.252941176470588f * (source->GetLevel() - 1);
					return 8.0f + 4.0f * source->GetLevel() +
						source->GetAttackDamage() +
						source->GetAbilityPower() * 0.5f +
						(source->GetMagicResist() - baseMResist) * 0.4f;
				}
				return 0.0f;
			},
			"Galio",
		});
	}

	if (Game::IsHeroInGame("Garen")) {
		StaticPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("garenq")) {
					return -5.0f + 35.0f * source->GetSpell((unsigned char)SpellSlot::Q).Level + 0.4f * source->GetAttackDamage();
				}
				return 0.0f;
			},
			"Garen",
		});
	}

	if (Game::IsHeroInGame("Gnar")) {
		DynamicPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->GetBuffCount("gnarwproc") == 2) {
					int wLvl = source->GetSpell((unsigned char)SpellSlot::W).Level;
					float gnarpassiveDmg = 10 * wLvl + target->GetHealth().Max * (0.04f + 0.02f * wLvl) + source->GetAbilityPower();
					if (source->IsMinion() && source->GetTeamID() == TEAM_JUNGLE) {
						return min(gnarpassiveDmg,50.0f + 50.0f * wLvl + source->GetAbilityPower());
					}
					return gnarpassiveDmg;
				}
				return 0.0f;
			},
			"Gnar"
		});
	}

	if (Game::IsHeroInGame("Gragas")) {
		DynamicPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("gragaswattackbuff")) {
					float gragasWPassive = -10.0f + 30.0f * source->GetSpell((unsigned char)SpellSlot::W).Level +
						target->GetHealth().Max * 0.08f +
						0.5f * source->GetAbilityPower();
					if (target->IsMinion() && target->GetTeamID() == TEAM_JUNGLE) {
						return min(300, gragasWPassive);
					}
					return gragasWPassive;
				}
				return 0.0f;
			},
			"Gragas"
		});
	}

	if (Game::IsHeroInGame("Hecarim")) {
		StaticPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("hecarimrampspeed")) {
					return 20.0f + 70.0f * source->GetSpell((unsigned char)SpellSlot::E).Level + source->GetAttackDamage();
				}
				return 0.0f;
			},
			"Hecarim",
		});
	}

	if (Game::IsHeroInGame("Illaoi")) {
		StaticPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("IllaoiW")) {
					return 0.025f + 0.005f * source->GetSpell((unsigned char)SpellSlot::W).Level + 0.02f * floor(source->GetAttackDamage() / 100);
				}
				return 0.0f;
			},
			"Illaoi",
		});
	}

	if (Game::IsHeroInGame("Irelia")) {
		StaticPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				int buffstack = source->GetBuffCount("ireliapassivestacks");
				if (buffstack > 0) {
					return buffstack * std::vector<float>{ 4,4.8f, 5.5f, 6.3f, 7.1f, 7.8f, 8.6f,9.4f,10.1f, 10.9f, 11.6f, 12.4f,13.2f, 13.9f,14.7f, 15.5f,16.2f, 17 }[min(source->GetLevel() - 1, 17)] + 0.04f * source->GetAttackDamage();
				}
				return 0.0f;
			},
			"Irelia",
		});
	}

	if (Game::IsHeroInGame("Jarvan")) {
		DynamicPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				float targetHP = target->GetHealth().Current;
				float j4dmg = targetHP * 0.08f;
				if (!target->HasBuff("jarvanivmartialcadencecheck")) {
					if (target->IsHero()) {
						if (targetHP < 250.0f) {
							return 20.0f;
						}
						return max(20.0f, j4dmg);
					}
					if (target->IsMinion()) {
						return max(20.0f, min(400.0f, j4dmg));
					}
				}
				return 0.0f;
			},
			"JarvanIV"
		});
	}

	if (Game::IsHeroInGame("Jax")) {
		StaticPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("JaxEmpowerTwo")) {
					return 5.0f + 35.0f * source->GetSpell((unsigned char)SpellSlot::W).Level + 0.6f * source->GetAbilityPower();
				}
				return 0.0f;
			},
			"Jax"
		});
	}

	if (Game::IsHeroInGame("Jayce")) {
		StaticPassives.push_back(DamagePassive{
			PercentPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("jaycehypercharge")) {
					return 0.7f + 0.08f * source->GetSpell((unsigned char)SpellSlot::W).Level;
				}
				return 1.0f;
			},
			"Jayce",
		});
	}

	if (Game::IsHeroInGame("Jhin")) {
		DynamicPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("jhinpassiveattackbuff")) {
					int lvl = source->GetLevel();
					float missingHP = target->GetHealth().Max - target->GetHealth().Current;
					return (lvl < 6 ? 0.15f : lvl < 11 ? 0.2f : 0.25f) * missingHP;
				}
				return 0.0f;
			},
			"Jhin"
		});
	}

	if (Game::IsHeroInGame("Jinx")) {
		StaticPassives.push_back(DamagePassive{
			PercentPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("JinxQ")) {
					return 1.1f;
				}
				return 1.0f;
			},
			"Jinx",
		});
	}

	if (Game::IsHeroInGame("Kaisa")) {
		DynamicPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				int lvl = source->GetLevel();
				float passivedmg = (lvl < 3 ? 4.0f : lvl < 6 ? 5.0f : lvl < 9 ? 6.0f : lvl < 11 ? 7.0f : lvl < 14 ? 8.0f : lvl < 17 ? 9.0f : 10.0f) + 0.1f * source->GetAbilityPower();
				float passiveDamagePerPlasmaStack = ((lvl < 4 ? 1.0f : lvl < 8 ? 2.0f : lvl < 12 ? 3.0f : lvl < 16 ? 4.0f : 5) + 0.025f * source->GetAbilityPower()) *target->GetBuffCount("kaisapassivemarker");
				return passivedmg + passiveDamagePerPlasmaStack;
			},
			"Kaisa"
		});

		DynamicPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target)
			{
				if (target->GetBuffCount("kaisapassivemarker") == 4)
				{
					float missingHP = target->GetHealth().Max - target->GetHealth().Current;
					float passiveDamage = missingHP * (0.15f + 0.025f * source->GetAbilityPower() / 100);
					if (target->IsMinion())
					{
						if (target->GetTeamID() == TEAM_JUNGLE)
						{
							return min(400, passiveDamage);
						}
						return passiveDamage;
					}
					if (target->IsHero())
					{
						return passiveDamage;
					}
				}
				return 0.0f;
			},
			"Kaisa"
		});
	}

	if (Game::IsHeroInGame("Kalista")) {
		// Kalista SpearBuddy just for the Spearbuddy
		DynamicPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				auto buff = target->GetBuff("kalistacoopstrikemarkbuff");
				if (buff.StartTime != NULL && source->HasBuff("kalistacoopstrikeally")) {
					auto buffCaster = (AIHeroClient*)pSDK->EntityManager->GetObjectFromPTR(buff.Caster);
					if (buffCaster != NULL) {
						auto wLvl = buffCaster->GetSpell((unsigned char)SpellSlot::W).Level;
						float basedmg = 5.0f + 5.0f * wLvl;
						float dmg = (0.025f + 0.025f * wLvl) * target->GetHealth().Max + basedmg;
						if (target->IsMinion()) {
							float maxdmg = (float)(75.0f + 25.0f * wLvl == 1 ? 0 : wLvl);
							return min(maxdmg, dmg);
						}
						return dmg;
					}
				}
				return 0.0f;
			}
		});

		// Kalista just for Kalista
		DynamicPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (target->HasBuff("kalistacoopstrikemarkally")) {
					float passiveDamage = (0.025f + 0.025f * source->GetSpell((unsigned char)SpellSlot::W).Level);
					if (target->IsMinion()) {
						int lvl = source->GetSpell((unsigned char)SpellSlot::W).Level;
						if (target->GetTeamID() == TEAM_ENEMY && target->GetHealth().Current < 125) {
							return target->GetHealth().Current;
						}
						float maxdmg = (float)(75.0f + 25.0f * lvl == 1 ? 0 : lvl);
						return min(maxdmg, passiveDamage);
					}
					return passiveDamage;
				}
				return 0.0f;
			},
			"Kalista"
		});

		// Kalista aa dmg reduction 90%
		StaticPassives.push_back(DamagePassive{
			PercentPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				return 0.9f;
			},
			"Kalista",
		});
	}

	if (Game::IsHeroInGame("Kassadin")) {
		StaticPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (!source->HasBuff("NetherBlade") && source->GetSpell((unsigned char)SpellSlot::W).Level > 0) {
					return 20.0f + 0.1f * source->GetAttackDamage();
				}
				return 0.0f;
			},
			"Kassadin",
		});

		StaticPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("NetherBlade")) {
					return 15.0f + 25.0f * source->GetSpell((unsigned char)SpellSlot::W).Level + 0.8f * source->GetAbilityPower();
				}
				return 0.0f;
			},
			"Kassadin",
		});
	}

	if (Game::IsHeroInGame("Kayle")) {
		StaticPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->GetSpell((unsigned char)SpellSlot::E).Level > 0) {
					float baseEPassive = 5.0f + 5.0f * source->GetSpell((unsigned char)SpellSlot::E).Level + 0.15f * source->GetAbilityPower();
					if (source->HasBuff("JudicatorRighteousFury")) {
						return baseEPassive * 2;
					}
					return baseEPassive;
				}
				return 0.0f;
			},
			"Kayle",
		});
	}

	//TODO: Rewrite Kled Later (This has to be messed up)
	if (Game::IsHeroInGame("Kled")) {
		DynamicPassives.push_back(DamagePassive{
			PercentPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (target->IsHero() && source->GetSpell((unsigned char)SpellSlot::Q).Name == "KledRiderQ") {
					auto sourcePos = source->GetPosition();
					auto gameObjects{ pSDK->EntityManager->GetParticles(80.0f, &sourcePos) };
					for (auto&[_, obj] : gameObjects) {
						std::string name = obj->GetName();
						if (name.find("Kled_") != std::string::npos && name.find("_W_4th_ready") != std::string::npos) {
							float distance = HUGE_VALF;
							AIHeroClient* closest = NULL;
							auto AllyHeroes{ pSDK->EntityManager->GetAllyHeroes() };
							for (auto&[_, hero] : AllyHeroes) {
								float uDist = hero->Distance(&(obj->GetPosition()));
								if (uDist < distance) {
									distance = uDist;
									closest = hero;
								}
							}
							if (closest == source) {
								return 0.8f;
							}
							break;
						}
					}
				}
				return 1.0f;
			},
			"Kled"
		});

		DynamicPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (target->IsHero() && strcmp(source->GetSpell((unsigned char)SpellSlot::Q).Name, "KledRiderQ") == 0) {
					auto sourcePos = source->GetPosition();
					auto gameObjects{ pSDK->EntityManager->GetParticles(80.0f, &sourcePos) };
					for (auto&[_, obj] : gameObjects) {
						std::string name{ obj->GetName() };
						if (name.find("Kled_") != std::string::npos && name.find("_W_4th_ready") != std::string::npos) {
							float distance = HUGE_VALF;
							AIHeroClient* closest = NULL;
							auto AllyHeroes{ pSDK->EntityManager->GetAllyHeroes() };
							for (auto&[_, hero] : AllyHeroes) {
								float uDist = hero->Distance(&(obj->GetPosition()));
								if (uDist < distance) {
									distance = uDist;
									closest = hero;
								}
							}
							if (closest == source) {
								int wLvl = source->GetSpell((unsigned char)SpellSlot::W).Level;
								float kledpassivedmg = 10.0f + 10.0f * wLvl;
								if (target->IsTurret() || target->IsInhibitor() || target->IsHQ()) {
									return kledpassivedmg;
								}
								float scaling = 0.04f + 0.005f * wLvl + target->GetHealth().Max * (0.05f * floor(source->GetBonusAttackDamage() / 100.0f));
								float totaldmg = kledpassivedmg + scaling;

								if (target->IsMinion() && target->GetTeamID() == TEAM_JUNGLE) {
									return min(200.0f, totaldmg);
								}
								return totaldmg;
							}
							break;
						}
					}
				}
				return 0.0f;
			},
			"Kled"
		});
	}

	if (Game::IsHeroInGame("KogMaw")) {
		DynamicPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("kogmawbioarcanebarrage")) {
					float basedmg = target->GetHealth().Max * (0.0225f + 0.0075f * source->GetSpell((unsigned char)SpellSlot::W).Level + 0.01f * floor(source->GetAbilityPower() / 100.0f));
					if (target->IsMinion()) {
						return min(100.0f, basedmg);
					}
					return basedmg;
				}
				return 0.0f;
			},
			"KogMaw"
		});
	}

	if (Game::IsHeroInGame("Leona")) {
		DynamicPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				auto leonaPassive = target->GetBuff("LeonaSunlight");
				if (leonaPassive.StartTime != NULL) {
					auto leonaPassiveCaster = (AIHeroClient*)pSDK->EntityManager->GetObjectFromPTR(leonaPassive.Caster);
					if (leonaPassiveCaster != NULL) {
						return 18.0f + 7.0f * leonaPassiveCaster->GetLevel();
					}
				}
				return 0.0f;
			}
		});
	}

	if (Game::IsHeroInGame("Lucian")) {
		DynamicPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("LucianPassiveBuff")) {
					int lvl = source->GetLevel();
					float basedmg = (lvl < 7 ? 0.5f : lvl < 13 ? 0.55f : 0.6f) *source->GetAttackDamage();;
					if (target->IsMinion()) {
						basedmg = source->GetAttackDamage();
					}
					return basedmg;
				}
				return 0.0f;
			},
			"Lucian"
		});
	}

	if (Game::IsHeroInGame("Lux")) {
		StaticPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("luxilluminatingfraulein")) {
					return 10.0f + 10.0f * (source->GetLevel()) + 0.2f * source->GetAbilityPower();
				}
				return 0.0f;
			},
			"Lux",
		});
	}

	if (Game::IsHeroInGame("MasterYi")) {
		StaticPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("floatstrike")) {
					return 0.5f * source->GetAttackDamage();
				}
				return 0.0f;
			},
			"MasterYi",
		});

		StaticPassives.push_back(DamagePassive{
			FlatTrue,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("wujustylesuperchargedvisual")) {
					return 10.0f + 0.8f * source->GetSpell((unsigned char)SpellSlot::E).Level + 0.35f * source->GetBonusAttackDamage();
				}
				return 0.0f;
			},
			"MasterYi",
		});
	}

	if (Game::IsHeroInGame("MissFortune")) {
		DynamicPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				auto targPos {target->GetPosition()};
				auto gameObject {pSDK->EntityManager->GetParticles(150.0f, &targPos)};
				for (auto&[_, obj] : gameObject) {
					std::string name{ obj->GetName() };
					if (name.find("MissFortune_") != std::string::npos && name.find("_P_Mark") != std::string::npos) {
						if (obj->Distance(target) < target->GetBoundingRadius() / 2) {
							int lvl = source->GetLevel();
							float mfPassiveDamage = (lvl < 4 ? 0.5f : lvl < 7 ? 0.6f : lvl < 9 ? 0.7f : lvl < 11 ? 0.8f : lvl < 13 ? 0.9f : 1.0f) * source->GetAttackDamage();
							if (target->IsMinion() && target->GetTeamID() != TEAM_JUNGLE) {
								return mfPassiveDamage / 2;
							}
							return mfPassiveDamage;
						}
						break;
					}
				}
				return 0.0f;
			},
			"MissFortune"
		});
	}

	if (Game::IsHeroInGame("Nami")) {
		StaticPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target) {
				auto namiEbuff = source->GetBuff("NamiE");
				if (namiEbuff.StartTime != NULL) {
					auto veryNiceNami = (AIHeroClient*)pSDK->EntityManager->GetObjectFromPTR(namiEbuff.Caster);
					if (veryNiceNami != NULL) {
						return 10.0f + 15.0f *  veryNiceNami->GetSpell((unsigned char)SpellSlot::E).Level +	veryNiceNami->GetAbilityPower() * 0.2f;
					}
				}
				return 0.0f;
			},
		});
	}
		   	
	if (Game::IsHeroInGame("Nasus")) {
		StaticPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target) {
				if (source->HasBuff("NasusQ")) {
					return 10.0f + 20.0f * source->GetSpell((unsigned char)SpellSlot::Q).Level + source->GetBuff("NasusQStacks").Stacks;
				}
				return 0.0f;
			},
			"Nasus"
		});
	}

	if (Game::IsHeroInGame("Nautilus")) {
		StaticPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target)	{
				if (source->HasBuff("nautiluspassivecheck")){
					return 2.0f + 6.0f * source->GetLevel();
				}
				return 0.0f;
			},
			"Nautilus"
		});

		// Nautilus
		StaticPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target)	{
				if (source->HasBuff("nautiluspiercinggazeshield"))	{
					return 10.0f + 5.0f * source->GetSpell((unsigned char)SpellSlot::W).Level + 0.2f * source->GetAbilityPower();
				}
				return 0.0f;
			},
			"Nautilus"
		});
	}

	if (Game::IsHeroInGame("Nidalee")) {
		DynamicPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target)	{
				if (source->HasBuff("takedown"))	{
					float nidaQPerc = 2.0f + 0.25f * source->GetSpell((unsigned char)SpellSlot::R).Level;
					float nidaQDmg = 5.0f + 25.0f * source->GetSpell((unsigned char)SpellSlot::R).Level + 0.75f * source->GetAttackDamage() + 0.4f * source->GetAbilityPower();
					float totaldmg = nidaQDmg * nidaQPerc;
					if (target->HasBuff("nidaleepassivehunted")){
						return totaldmg * 1.4f;
					}
					return totaldmg;
				}
				return 0.0f;
			},
			"Nidalee"
		});
	}

	if (Game::IsHeroInGame("Nocturne")) {
		StaticPassives.push_back(DamagePassive{
			PercentPhysical,
			[](AIHeroClient* source, AIBaseClient* target){
				if (source->HasBuff("umbrablades"))	{
					return 1.2f;
				}
				return 1.0f;
			},
			"Nocturne"
		});
	}

	if (Game::IsHeroInGame("Orianna")) {
		StaticPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target)	{
				int lvl = source->GetLevel();
				float basePassiveDamage = (lvl < 4 ? 10.0f : lvl < 7 ? 18.0f : lvl < 10 ? 26.0f : lvl < 13 ? 34.0f : lvl < 16 ? 42.0f : 50.0f) + 0.15f * source->GetAbilityPower();
				float passiveStackBonusDmg = ((lvl < 4 ? 2.0f : lvl < 7 ? 3.6f : lvl < 10 ? 5.2f : lvl < 13 ? 6.8f : lvl < 16 ? 8.4f : 10.0f) + 0.03f * source->GetAbilityPower()) *source->GetBuffCount("orianapowerdaggerdisplay");
				return basePassiveDamage + passiveStackBonusDmg;
			},
			"Orianna"
		});
	}

	if (Game::IsHeroInGame("Ornn")) {
		DynamicPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target)	{
				if (target->HasBuff("ornnvulnerabledebuff")){
					int lvl = source->GetLevel();
					return target->GetHealth().Max * (lvl < 3 ? 0.7f : lvl < 6 ? 0.8f : lvl < 9 ? 0.9f : lvl < 12 ? 0.1f : lvl < 15 ? 0.11f : lvl < 18 ? 0.12f : 0.13f);
				}
				return 0.0f;
			},
			"Ornn"
		});
	}

	if (Game::IsHeroInGame("Quinn")) {
		DynamicPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target){
				if (target->HasBuff("quinnw")){
					int lvl = source->GetLevel();
					return 5.0f + 5.0f * lvl + (0.14f + 0.02f * source->GetLevel()) + source->GetAttackDamage();
				}
				return 0.0f;
			},
			"Quinn"
		});
	}

	if (Game::IsHeroInGame("Sejuani")) {
		DynamicPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target){
				if (target->HasBuff("sejuanistun"))	{
					int lvl = source->GetLevel();
					float detonateDmg = target->GetHealth().Max *  (lvl < 7 ? 0.1f : lvl < 14 ? 0.15f : 0.2f);
					if (target->IsHero()){
						return detonateDmg;
					}
					if (target->IsMinion())	{
						if (target->GetTeamID() == TEAM_JUNGLE)	{
							return min(300.0f, detonateDmg);
						}
						return detonateDmg;
					}
				}
				return 0.0f;
			},
			"Sejuani"
		});
	}

	if (Game::IsHeroInGame("Shaco")) {
		StaticPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target){
				if (source->HasBuff("deceive"))	{
					return 10.0f * source->GetSpell((unsigned char)SpellSlot::Q).Level + 0.4f * source->GetBonusAttackDamage() + 0.3f * source->GetAbilityPower();
				}
				return 0.0f;
			},
			"Shaco"
		});
	}

	if (Game::IsHeroInGame("Teemo")) {
		DynamicPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target)	{
				int eLvl = source->GetSpell((unsigned char)SpellSlot::E).Level;
				if (source->GetSpell(2).Learned){
					if (!target->IsTurret() && target->IsHQ() && target->IsInhibitor())	{
						return 10.0f * eLvl + 0.3f * source->GetAbilityPower();
					}
				}
				return 0.0f;
			},
			"Teemo"
		});
	}

	if (Game::IsHeroInGame("Thresh")) {
		StaticPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target)	{
				int eLvl = source->GetSpell((unsigned char)SpellSlot::E).Level;
				if (source->GetSpell(2).Learned) {
					float multiplier = 1.0f;
					float passivedmg = source->GetBuffStacks("threshpassivesoulsgain") + (0.75f + 0.25f * eLvl + source->GetAttackDamage());
					if (source->HasBuff("threshepassive3")) {
						multiplier *= 0.5f;
					}
					else if (source->HasBuff("threshepassive2")) {
						multiplier *= 0.333f;
					}
					else if (source->HasBuff("threshepassive1"))
					{
						multiplier *= 0.25f;
					}
					return passivedmg * multiplier;
				}
				return 0.0f;
			},
			"Thresh"
		});
	}

	if (Game::IsHeroInGame("TwistedFate")) {
		StaticPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target)	{
				int wLvl = source->GetSpell((unsigned char)SpellSlot::W).Level;
				if (source->HasBuff("bluecardpreattack")){
					return 20.0f + 20.0f * wLvl + source->GetAttackDamage() + 0.5f * source->GetAbilityPower();
				}

				if (source->HasBuff("redcardpreattack")){
					return 15.0f + 15.0f * wLvl + source->GetAttackDamage() + 0.5f * source->GetAbilityPower();
				}

				if (source->HasBuff("goldcardpreattack")){
					return 7.5f + 7.5f * wLvl + source->GetAttackDamage() + 0.5f * source->GetAbilityPower();
				}
				return 0.0f;
			},
			"TwistedFate"
		});	

		StaticPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target){
				if (source->HasBuff("cardmasterstackparticle"))	{
					int eLvl = source->GetSpell((unsigned char)SpellSlot::E).Level;
					return 30.0f + 25.0f * eLvl + source->GetAttackDamage() + 0.5f * source->GetAbilityPower();
				}
				return 0.0f;
			},
			"TwistedFate"
		});
	}

	if (Game::IsHeroInGame("Varus")) {
		StaticPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target){
				auto Spell{source->GetSpell((unsigned char)SpellSlot::W)};
				if (Spell.Learned){
					return 3.5f + 3.5f * Spell.Level + 0.25f * source->GetAbilityPower();
				}
				return 0.0f;
			},
			"Varus"
		});
	}

	if (Game::IsHeroInGame("Vayne")) {
		StaticPassives.push_back(DamagePassive{
			FlatPhysical,
			[](AIHeroClient* source, AIBaseClient* target){
				if (source->HasBuff("vaynetumblebonus")){
					return (0.45f + 0.5f * source->GetSpell((unsigned char)SpellSlot::Q).Level) * source->GetAttackDamage();
				}
				return 0.0f;
			},
			"Vayne"
		});


		DynamicPassives.push_back(DamagePassive{
			FlatTrue,
			[](AIHeroClient* source, AIBaseClient* target)	{
				if (target->GetBuffStacks("VayneSilveredDebuff") == 2){
					float wPassivedmg = target->GetHealth().Max * (0.015f + 0.025f * source->GetSpell((unsigned char)SpellSlot::W).Level);
					float mindmg = 35.0f + (15.0f * source->GetSpell((unsigned char)SpellSlot::W).Level);
					if (target->IsMinion())	{
						if (target->GetTeamID() == TEAM_JUNGLE)	{
							return min(200.0f, max(mindmg, wPassivedmg));
						}
						return max(mindmg, wPassivedmg);
					}
					if (target->IsHero()){
						return max(mindmg, wPassivedmg);
					}
				}
				return 0.0f;
			},
			"Vayne"
		});
	}

	if (Game::IsHeroInGame("Viktor")) {
		DynamicPassives.push_back(DamagePassive{
			FlatMagical,
			[](AIHeroClient* source, AIBaseClient* target)	{
				if (target->HasBuff("ViktorPowerTransfer"))	{
					return -10.0f + (30.0f * source->GetSpell((unsigned char)SpellSlot::Q).Level) + source->GetAttackDamage() + 0.5f * source->GetAbilityPower();
				}
				return 0.0f;
			},
			"Viktor"
		});
	}	
}
#pragma endregion